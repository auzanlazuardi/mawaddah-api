<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;

class GalleryController extends Controller
{
    public function index()
    {
    	$data = Gallery::all();

    	return response()->json([
    		'responseCode' 		=> '1',
    		'responseMessage'	=> 'Berhasil mengambil data',
    		'responseContents'	=> $data
    	]);
    }
}
